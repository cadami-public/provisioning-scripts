#!/bin/sh

log=/var/log/engine-install-main.log
docker_version=20.10

# setup logging to file
rm -f "$log"
mkdir -p `dirname "$log"`
exec 1>>"$log" 2>>"$log"

set -ex

install_docker() {
    curl -L "https://releases.rancher.com/install-docker/$docker_version.sh" | sh -
}

echo "## Start engine-install"

cd /

echo "## Waiting for cloud-init"

cloud-init status --wait

echo "## Apply sysctl changes for container workloads and networking"

cat >>/etc/sysctl.conf <<EOF
net.ipv4.ip_forward=1
net.ipv4.neigh.default.gc_thresh1=4096
net.ipv4.neigh.default.gc_thresh2=16384
net.ipv4.neigh.default.gc_thresh3=32768
net.ipv6.neigh.default.gc_thresh1=4096
net.ipv6.neigh.default.gc_thresh2=16384
net.ipv6.neigh.default.gc_thresh3=32768
EOF

sysctl -p

echo "## Real docker engine install"

install_docker || install_docker

cat <<EOF
## Install custom packages:
##    - qemu-user-static            [crossarch container support]
##    - binfmt-support              [crossarch container support]
##    - openvswitch-switch          [mininet and mininet-wifi]
##    - linux-modules-extra-...     [mac80211_hwsim for mininet-wifi]
EOF

export DEBIAN_FRONTEND=noninteractive
apt-get install -y --no-install-recommends \
        qemu-user-static \
        binfmt-support \
        openvswitch-switch \
        linux-modules-extra-$(uname -r)

echo "## disable unattended upgrades"

systemctl disable apt-daily-upgrade.timer || true
systemctl disable apt-daily.timer || true

echo "## Completed engine-install"

exit 0
